<?php
$l['restore'] = "กู้คืน";
$l['move_to_trash'] = "ย้ายไปถังขยะ";
$l['compose'] = "เขียน";
$l['trash'] = "ถังขยะ";
$l['inbox'] = "กล่องข้อความ";
$l['sent_messages'] = "ส่งข้อความ";
$l['receiver'] = "ผู้รับ";
$l['sender'] = "ผู้ส่ง";
$l['actions'] = "การทำงาน";
$l['subject'] = "หัวข้อ";
$l['message'] = "ข้อความ";
$l['to'] = "ถึง";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "ไม่พบข้อความ";
$l['no_more_messages_found'] = "ไม่สามารถหาข้อความเพิ่มเติมได้";
$l['messages'] = "ข้อความ";
$l['send_message_banned'] = "คุณไม่สามารถส่งข้อความได้เนื่องจากคุณอยู่ในสถานะแบน";
