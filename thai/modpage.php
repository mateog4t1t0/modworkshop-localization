<?php

$l['mydownloads_being_updated'] = 'กำลังอัปเดต';
$l['mydownloads_meta_by'] = '{1} โดย {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'คุณไม่สามารถโหวตให้กับ Mod ตนเองได้.';
$l['mydownloads_collaborators_desc'] = 'ทีมคือผู้ที่สามารถแก้ไข Mod ได้. แต่ไม่สามารถลบ, เปลี่ยนเจ้าของ, หรือแก้ไขทีมได้';
$l['mydownloads_comment_banned'] = "คุณไม่สามารถแสดงความเห็นได้หากโดนแบนอยู่.";
$l['mydownloads_delete_confirm'] = "คุณแน่ใจหรือไม่ที่ต้องการลบความเห็นนี้?";
$l['mydownloads_download_description'] = "คำอธิบาย";
$l['mydownloads_download_is_suspended'] = '
Mod นี้ถูกระงับและสามารถเห็นได้เพียงแค่ทีมงานและผู้ดูแลเท่านั้น.
<br/>การระงับเป็นการระงับแบบชั่วคราวเพื่อตรวจสอบหรือระงับถาวรหากไม่ตรงกับข้อตกลงการใช้งาน <a style="text-decoration:underline;" href="/rules">กฏ</a>.
<br/>คุณควรที่จะติดต่อทีมงานหรือถ้าหาก Mod ของคุณถูกอัปเดตและเป็นไปตามกฏคุณสามารถส่ง "ยกเลิกการระงับ" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">ได้ที่นี่</a>.';
$l['mydownloads_download_changelog'] = 'การเปลี่ยนแปลง';
$l['mydownloads_download'] = "ดาวน์โหลด";
$l['mydownloads_license'] = 'การอนุญาต';
$l['mydownloads_report_download'] = 'รายงาน Mod';
$l['show_download_link_warn'] = 'กรุณาระวังลิงค์ที่น่าสงสัย. ถ้าหากคุณคิดว่าลิงค์ไม่ปลอดภัย, กรุณาติดต่อผู้ดูแล';
$l['show_download_link'] = 'แสดงลิงค์ดาวน์โหลด';
$l['show_files'] = 'แสดงไฟล์';
$l['submitted_by'] = 'ส่งมาโดย';
$l['subscribe_commnet_help'] = 'แจ้งเตือนหากมีการแสดงความเห็น';
$l['follow_mod_help'] = 'ติดตาม Mod นี้เพื่อแสดงการแจ้งเตือนเมื่อ Mod ได้รับการอัปเดต';
$l['mydownloads_download_comments'] = "การแสดงความคิดเห็น";
$l['mydownloads_unsuspend_it'] = 'ยกเลิกการระงับ';
$l['mydownloads_suspend_it'] = 'ระงับ';
$l['mydownloads_files_alert'] = '<strong>ไม่มีไฟล์</strong> | เมื่อ Mod ไม่มีไฟล์, Mod จะถูกซ่อนไว้.';
$l['mydownloads_files_alert_waiting'] = '<strong>ไม่มีไฟล์ที่ได้รับการยืนยัน</strong> | Mod จะถูกซ่อนไว้จนว่าจะได้รับการยืนยันไฟล์จากผู้ดูแล.';
$l['share'] = 'แบ่งปัน';
