<?php
$l['myalerts_pm'] = '{1} ได้ส่งข้อความส่วนตัวและมีหัวข้อว่า <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} ได้กล่าวถึงคุณ <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} ตอบกลับหัวข้อของคุณ d <b>"{2}"</b>. อาจจะมีโพสต์ต่อจากนี้.';
$l['myalerts_subscribed_thread'] = '{1} replied to your subscribed thread <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} เรทให้หัวข้อของคุณ <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} ได้เข้าร่วมโหวตของคุณใน <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} ได้รับการอัปเดตตาม Mod  {2}";
$l['alert_subbed_discussion_modpage'] = "{1} ได้แสดงความคิดเห็นใน Mod {2} ที่คุณได้สมัครสมาชิก";
$l['no_alerts_found'] = "ไม่พบการแจ้งเตือน";
$l['no_more_alerts_found'] = "ไม่สามารถรับการแจ้งเตือนได้อีก";
$l['notifications'] = 'การแจ้งเตือน';
$l['browse_all_notifications'] = 'ดูการแจ้งเตือนทั้งหมด';
