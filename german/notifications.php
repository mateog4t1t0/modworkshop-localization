<?php
$l['myalerts_pm'] = '{1} hat dir eine Nachricht mit dem Betreff <b>"{2}"</b> gesendet.';
$l['myalerts_quoted'] = '{1} hat dich <b>"{2}"</b> zitiert.';
$l['myalerts_post_threadauthor'] = '{1} hat auf ihren Thread <b>"{2}"</b> geantwortet. Es könnten mehrere Posts nach diesem folgen.';
$l['myalerts_subscribed_thread'] = '{1} hat auf den von ihnen abbonierten Thread <b>"{2}"</b> geantwortet.';
$l['myalerts_rated_threadauthor'] = '{1} hat ihren Thread <b>"{2}"</b> bewertet.';
$l['myalerts_voted_threadauthor'] = '{1} hat in deiner Umfrage in <b>"{2}"</b> abgestimmt.';
$l['alert_mod_updated'] = "{1} hat die verfolgte Mod {2} aktualisiert";
$l['alert_subbed_discussion_modpage'] = "{1} hat die Mod {2} kommentiert, die du abboniert hast";
$l['no_alerts_found'] = "Keine Benachrichtigungen gefunden";
$l['no_more_alerts_found'] = "Konnte keine weiteren Benachrichtigungen finden";
$l['notifications'] = 'Benachrichtigungen';
$l['browse_all_notifications'] = 'Zeige alle Benachrichtigungen';
