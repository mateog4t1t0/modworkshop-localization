<?php
$l['restore'] = "Wiederherstellen";
$l['move_to_trash'] = "In den Papierkorb verschieben";
$l['compose'] = "Verfassen";
$l['trash'] = "Papierkorb";
$l['inbox'] = "Eingang";
$l['sent_messages'] = "Gesendete Nachrichten";
$l['receiver'] = "Empfänger";
$l['sender'] = "Absender";
$l['actions'] = "Aktionen";
$l['subject'] = "Betreff";
$l['message'] = "Nachricht";
$l['to'] = "An";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "Keine Nachrichten gefunden";
$l['no_more_messages_found'] = "Konnte keine weiteren Nachrichten finden";
$l['messages'] = "Nachrichten";
$l['send_message_banned'] = "Du kannst keine Nachrichten senden, während du gebannt bist.";
