<?php

$l['already_support'] = "You already support us. Thank you! ❤"; 
$l['support_mws_details'] = "
<h4>Hate ads? Want to support us?</h4>

No advertisements.<br>
A unique role for Discord, and ModWorkshop website.<br>
A badge showing your support.<br>
A unique color for your name in the website, and the Discord server.<br>
";
$l['support_mws_extra_details'] = "
*On Discord, please ask a moderator for the custom supporter role.<br>
*For information about refunds, or extending your payment plan, please contact an administrator.<br>
*English is the primary language used on ModWorkshop, and we'll be able to help you faster if using said language.<br>
";
$l['support_mws_not_logged'] = 'In order to support us, you must first be logged in. <a href="/login">Sign in through Steam</a>';
