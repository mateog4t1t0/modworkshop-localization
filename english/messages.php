<?php
$l['restore'] = "Restore";
$l['move_to_trash'] = "Move to Trash Bin";
$l['compose'] = "Compose";
$l['inbox'] = "Inbox";
$l['trash'] = "Trash Bin";
$l['sent_messages'] = "Sent Messages";
$l['receiver'] = "Receiver";
$l['sender'] = "Sender";
$l['actions'] = "Actions";
$l['subject'] = "Subject";
$l['message'] = "Message";
$l['to'] = "To";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "No messages found";
$l['no_more_messages_found'] = "No more messages were found";
$l['messages'] = "Messages";
$l['send_message_banned'] = "You cannot send messages while you are banned.";
