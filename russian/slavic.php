<?php

//This is only for slavic languages at the moment as their spelling changes in certain cases requiring us to add an additional "plural".

$l['year2'] = 'года'; 
$l['months2'] = 'месяца'; 
$l['days2'] = 'дня';
$l['weeks2'] = 'недели';
$l['hours2'] = 'часа';
$l['minutes2'] = 'минуты';
$l['seconds2'] = 'секунды';
