<?php
$l['restore'] = "Restaurar";
$l['move_to_trash'] = "Mover para Lixeira";
$l['compose'] = "Enviar";
$l['trash'] = "Lixeira";
$l['inbox'] = "Caixa de entrada";
$l['sent_messages'] = "Mensagens enviadas";
$l['receiver'] = "Recebedor";
$l['sender'] = "Enviador";
$l['actions'] = "Ações";
$l['subject'] = "Assunto";
$l['message'] = "Mensagem";
$l['to'] = "Para";
$l['BCC'] = "BCC";
$l['no_messages_found'] = "Nenhuma mensagem achada";
$l['no_more_messages_found'] = "Não conseguiu achar mais mensagens";
$l['messages'] = "Mensagens";
$l['send_message_banned'] = "Você não pode achar mensagens enquanto estiver banido.";
