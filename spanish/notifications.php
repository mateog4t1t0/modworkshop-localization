<?php
$l['myalerts_pm'] = '{1} te ha enviado un mensaje privado titulado <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} te a mencionado en <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} a respondido a tu hilo <b>"{2}"</b>. Puede que haya más posts después de este.';
$l['myalerts_subscribed_thread'] = '{1} a respondido al un hilo que estas suscripto <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} a votado en tu hilo <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} a votado en tu votación en <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} a actualizado el siguiente mod {2}";
$l['alert_subbed_discussion_modpage'] = "{1} a comentado en el mod {2} en el cual estás suscripto";
$l['no_alerts_found'] = "No se encontraron notificaciones";
$l['no_more_alerts_found'] = "No se han encontrado más notificaciones";
$l['notifications'] = 'Notificaciones';
$l['browse_all_notifications'] = 'Busca Todas Las Notificaciones';
