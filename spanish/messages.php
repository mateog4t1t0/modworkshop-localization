<?php
$l['restore'] = "Restaurar";
$l['move_to_trash'] = "Mover a la basura";
$l['compose'] = "Componer";
$l['trash'] = "Basura";
$l['inbox'] = "Bandeja de entrada";
$l['sent_messages'] = "Mensajes enviados";
$l['receiver'] = "Recipiente";
$l['sender'] = "Remitente";
$l['actions'] = "Acciones";
$l['subject'] = "Tema";
$l['message'] = "Mensaje";
$l['to'] = "Para";
$l['BCC'] = "CCO";
$l['no_messages_found'] = "No hay mensajes encontrados";
$l['no_more_messages_found'] = "No se pudieron encontrar más mensajes";
$l['messages'] = "Mensajes";
$l['send_message_banned'] = "No puedes enviar mensajes mientras estás baneado.";