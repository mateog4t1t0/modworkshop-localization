<?php
$l['already_support'] = "Ya nos apoyas. ❤ ¡Muchas gracias! ❤"; 
$l['support_mws_details'] = "
<h4>¿Odias los anuncios de la pagina? ¿Quieres apoyarnos?</h4>

Sin anuncios.<br>
Un rol en Discord y en el sitio web.<br>
Una insignia mostrando tu apoyo.<br>
Un color personalizado a tu nombre tanto en el sitio web como en Discord*<br>
";
$l['support_mws_extra_details'] = "
*En Discord, tendrás que pedir a un moderador por el rol en color customizado.<br>
*Para pedir una devolución o extender el plan por favor hable con un administrador.<br>
*Nuestro sitio es mayoritariamente en inglés por lo que podremos dar nuestro mejor soporte en ese idioma.<br>
";
$l['support_mws_not_logged'] = 'Para poder procesar la donacion, primero debes de haber iniciado sesión en la página. <a href="/login">Comenzar Sesión por Steam</a>';