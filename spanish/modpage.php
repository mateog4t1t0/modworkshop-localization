<?php

$l['mydownloads_being_updated'] = 'Actualizando';
$l['mydownloads_meta_by'] = '{1} por {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'No puedes votar tus propios mods.';
$l['mydownloads_collaborators_desc'] = 'Los colaboradores son personas a quienes les das permisos de editar el mod. Ellos no pueden eliminar el mod, transferir la pertenencia del mismo, o editar sus colaboradores.';
$l['mydownloads_comment_banned'] = "No puedes comentar mientras estás suspendido.";
$l['mydownloads_delete_confirm'] = "¿Estás seguro que quieres eliminar este comentario?";
$l['mydownloads_download_description'] = "Descripción";
$l['mydownloads_download_is_suspended'] = 'Esta mod está suspendido y solo será visible para el autor y el personal administrative del sitio.
<br/>La suspensión podría ser temporalmente para su investigación, o permanentemente basado en la severidad, debido a la violación de las <a style="text-decoration:underline;" href="/rules">reglas</a>.
<br/>Si deseas contactar a un administrador respecto a esto, o si has actualizado el mod para que siga las reglas, puedes enviar una petición de “Un-Suspender aplicación” <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">aquí</a>.';
$l['mydownloads_download_changelog'] = 'Lista de cambios';
$l['mydownloads_download'] = "Descargar";
$l['mydownloads_license'] = 'Licencia';
$l['mydownloads_report_download'] = 'Reportar Mod';
$l['show_download_link_warn'] = 'Sé cauteloso de links sospechosos. Si piensas que el link es malicioso, por favor reporta el mod';
$l['show_download_link'] = 'Mostrar link(s) de descarga';
$l['show_files'] = 'Mostrar archivos';
$l['submitted_by'] = 'Enviado por';
$l['subscribe_commnet_help'] = 'Se notificado de cuando alguien comenta en esta sección de comentarios';
$l['follow_mod_help'] = 'Sigue a este mod para que sea mostrado en tu lista de mods a los que sigues y ser notificado de cuándo el mod es actualizado';
$l['mydownloads_download_comments'] = "Comentarios";
$l['mydownloads_unsuspend_it'] = 'Un-Suspender';
$l['mydownloads_suspend_it'] = 'Suspender';
$l['mydownloads_files_alert'] = '<strong>NO HAY ARCHIVOS</strong> | Debido a que el mod no tiene archivos, a sido marcado como invisible.';
$l['mydownloads_files_alert_waiting'] = '<strong>LOS ARCHIVOS NO HAN SIDO APROBADOS</strong> | Este mod será invisible hasta que los archivos que has subido sean aprobados.';
$l['share'] = 'Compartir';