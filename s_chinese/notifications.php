<?php
$l['myalerts_pm'] = '{1} 给您发了一条私信 <b>"{2}"</b>.';
$l['myalerts_quoted'] = '{1} 引用了您，在 <b>"{2}"</b>.';
$l['myalerts_post_threadauthor'] = '{1} 回复了您 <b>"{2}"</b>. 以后还会有更多回复的！';
$l['myalerts_subscribed_thread'] = '{1} 回复了您的订阅 <b>"{2}"</b>.';
$l['myalerts_rated_threadauthor'] = '{1} 赞了您的 <b>"{2}"</b>.';
$l['myalerts_voted_threadauthor'] = '{1} 为您的作品投票！投票作品： <b>"{2}"</b>.';
$l['alert_mod_updated'] = "{1} 更新了 {2} 快来看看吧！";
$l['alert_subbed_discussion_modpage'] = "{1} 在您订阅的 {2} 中发表了评论";
$l['no_alerts_found'] = "没有新通知了OwO";
$l['no_more_alerts_found'] = "没有更多了-.-";
$l['notifications'] = '通知';
$l['browse_all_notifications'] = '浏览所有通知';
